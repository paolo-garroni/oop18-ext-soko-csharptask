# OOP18-ext-soko-CSharpTask - EN

Translation from Java to C# of the model classes of the [Extendible Sokoban](https://bitbucket.org/paolo-garroni/oop18-ext-soko/src/master/) project.

This is the C# task of the OOP exam of my CS bachelor's degree, required to demonstrate a basic operative knowledge of C# programming.

# OOP18-ext-soko-CSharpTask - IT

Traduzione da Java a C# delle classi model del progetto [Extendible Sokoban](https://bitbucket.org/paolo-garroni/oop18-ext-soko/src/master/).

Si tratta del task C# dell'esame di OOP nel corso di studi in Ingegneria e Scienze informatiche, richiesto per dimostrare una conoscenza di base operativa della programmazione in C#.
