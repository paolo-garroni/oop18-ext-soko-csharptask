﻿using System;
using System.Collections.Generic;
using ExtendibleSokoban.model.levelsequence;

namespace ExtendibleSokoban.model
{
    /// <summary>
    /// An implementation of <see cref="IModel"/>
    /// </summary>
    public class Model : IModel
    {
        private ILevelSequence _levelSequence;
        
        /// <summary>
        /// Creates a new instance with an empty level sequence.
        /// </summary>
        public Model()
        {
            this.LevelSequence = new LevelSequence("");
        }

        /// <summary>
        /// The current level sequence in its current state.
        /// </summary>
        public ILevelSequence LevelSequence
        {
            get { return this._levelSequence; }
            set 
            {
                this._levelSequence = value;
                this.LevelSequenceInitialState = this.LevelSequence.CreateCopy();
            } 
        }
        
        /// <summary>
        /// The current level sequence in its initial state.
        /// </summary>
        public ILevelSequence LevelSequenceInitialState { get; private set; }

        /// <summary>
        /// The names of the levels.
        /// </summary>
        public List<String> LevelNames 
        { 
            get
            {
                List<String> levelNames = new List<String>();
                this.LevelSequence.Levels.ForEach(l => levelNames.Add(l.Name));
                return levelNames;
            }
        }
    }
}
