﻿using System;
using System.Collections.Generic;
using ExtendibleSokoban.model.levelsequence;

namespace ExtendibleSokoban.model
{
    /// <summary>
    /// The model, which is responsible for managing the level sequences and all of its components.
    /// </summary>
    public interface IModel
    {
        /// <summary>
        /// The current level sequence in its current state.
        /// </summary>
        ILevelSequence LevelSequence { get; set; }
        
        /// <summary>
        /// The current level sequence in its initial state.
        /// </summary>
        ILevelSequence LevelSequenceInitialState { get; }

        /// <summary>
        /// The names of the levels.
        /// </summary>
        List<String> LevelNames { get; }
    }
}
