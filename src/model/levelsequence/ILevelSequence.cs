using System;
using System.Collections.Generic;
using ExtendibleSokoban.model.levelsequence.level;

namespace ExtendibleSokoban.model.levelsequence
{
    /// <summary>
    /// An editable, ordered and iterable sequence of levels, with a name.
    /// </summary>
    public interface ILevelSequence
    {
        /// <summary>
        /// The name of the level sequence.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Gets the current ordered sequence of levels.
        /// </summary>
        List<ILevel> Levels { get; }

        /// <summary>
        /// Gets the current level in its current state.
        /// </summary>
        ILevel CurrentLevel { get; }

        /// <summary>
        /// Adds the given level.
        /// </summary>
        /// <param name="level">level the level to be added to the sequence</param>
        void Add(ILevel level);

        /// <summary>
        /// Removes the level with the given index from the sequence.
        /// </summary>
        /// <param name="i">the index of the level to be removed</param>
        void Remove(int i);

        /// <summary>
        /// Swap the levels with the specified indexes in the sequence.
        /// </summary>
        /// <param name="i">An index</param>
        /// <param name="j">Another index</param>
        void Swap(int i, int j);

        /// <summary>
        /// Clears the list removing all the levels.
        /// </summary>
        void Clear();

        /// <summary>
        /// Sets the next level as the current level and returns true. 
        /// If there is no level next, returns false.
        /// </summary>
        bool SetNextLevelAsCurrent();

        /// <summary>
        /// Restarts the current level restoring its initial state.
        /// </summary>
        void RestartCurrentLevel();

        /// <summary>
        /// Creates a copy of the level sequence.
        /// </summary>
        /// <returns>the level sequence copy</returns>
        ILevelSequence CreateCopy();
    }
}