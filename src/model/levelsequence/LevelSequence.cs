﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtendibleSokoban.model.levelsequence.level;
using ExtendibleSokoban.model.levelsequence.level.grid;

namespace ExtendibleSokoban.model.levelsequence
{
    /// <summary>
    /// An implementation of <see cref="ILevelSequence"/>.
    /// </summary>
    class LevelSequence : ILevelSequence
    {
        private IEnumerator<ILevel> iterator;

        /// <summary>
        /// Creates a level sequence with an empty grid and a name.
        /// </summary>
        /// <param name="name">the level sequence name</param>
        public LevelSequence(String name)
        {
            this.Name = name;
            this.Levels = new List<ILevel>();
            this.iterator = this.Levels.GetEnumerator();
            this.CurrentLevel = this.iterator.Current;
        }

        /// <summary>
        /// Creates a new level sequence using the given name and levels.
        /// </summary>
        /// <param name="name">the name</param>
        /// <param name="levels">the levels</param>
        public LevelSequence(String name, List<ILevel> levels) 
        {
            this.Name = name;
            this.Levels = levels;
            this.iterator = this.Levels.GetEnumerator();
            this.CurrentLevel = this.iterator.Current;
        }

        /// <summary>
        /// The name of the level sequence.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the current ordered sequence of levels.
        /// </summary>
        public List<ILevel> Levels { get; }

        /// <summary>
        /// Gets the current level in its current state.
        /// </summary>
        public ILevel CurrentLevel { get; private set; }

        /// <summary>
        /// Adds the given level.
        /// </summary>
        /// <param name="level">level the level to be added to the sequence</param>
        public void Add(ILevel level)
        {
            this.Levels.Add(level);
        }
        
        /// <summary>
        /// Removes the level with the given index from the sequence.
        /// </summary>
        /// <param name="i">the index of the level to be removed</param>
        public void Remove(int i)
        {
            this.Levels.RemoveAt(i);
        }

        /// <summary>
        /// Swap the levels with the specified indexes in the sequence.
        /// </summary>
        /// <param name="i">An index</param>
        /// <param name="j">Another index</param>
        public void Swap(int i, int j)
        {
            ILevel tmp = this.Levels[i];
            this.Levels[i] = this.Levels[j];
            this.Levels[j] = tmp;
        }
        
        /// <summary>
        /// Clears the list removing all the levels.
        /// </summary>
        public void Clear()
        {
            this.Levels.Clear();
        }
        
        /// <summary>
        /// Sets the next level as the current level and returns true. 
        /// If there is no level next, returns false.
        /// </summary>
        public bool SetNextLevelAsCurrent()
        {
            bool success = this.iterator.MoveNext();
            if (success)
            {
                this.CurrentLevel = this.iterator.Current;
            }
            return success;
        }

        /// <summary>
        /// Restarts the current level restoring its initial state.
        /// </summary>
        public void RestartCurrentLevel()
        {
            if (this.CurrentLevel != null)
            {
                this.CurrentLevel = new Level(this.CurrentLevel.Name, this.CurrentLevel.InitialGrid);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
        
        /// <summary>
        /// Creates a copy of the level sequence.
        /// </summary>
        /// <returns>the level sequence copy</returns>
        public ILevelSequence CreateCopy()
        {
            List<ILevel> levelsCopy = new List<ILevel>();
            this.Levels.ForEach(l => levelsCopy.Add(new Level(string.Copy(l.Name), new Grid(l.CurrentGrid))));
            return new LevelSequence(string.Copy(this.Name), levelsCopy);
        }

        /// <summary>
        /// Two level sequences are equal if their name and level sequence are equal.
        /// </summary>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((LevelSequence) obj);
        }

        /// <summary>
        /// Two level sequences are equal if their name and levels are equal.
        /// </summary>
        public bool Equals(LevelSequence other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Name == other.Name && Enumerable.SequenceEqual(Levels, other.Levels) && Equals(CurrentLevel, other.CurrentLevel);
        }

        /// <summary>
        /// A level sequence hashcode is computed upon its name and levels.
        /// </summary>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Levels != null ? Levels.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (CurrentLevel != null ? CurrentLevel.GetHashCode() : 0);
                return hashCode;
            }
        }

        /// <summary>
        /// Two level sequences are equal if their name and levels are equal.
        /// </summary>
        public static bool operator ==(LevelSequence left, LevelSequence right)
        {
            return Equals(left, right);
        }

        /// <summary>
        /// Two level sequences are not equal if their name and levels are not equal.
        /// </summary>
        public static bool operator !=(LevelSequence left, LevelSequence right)
        {
            return !Equals(left, right);
        }
    }
}
