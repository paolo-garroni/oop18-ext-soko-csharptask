﻿using System;
using System.Linq;
using ExtendibleSokoban.model.levelsequence.level.grid;
using ExtendibleSokoban.model.levelsequence.level.grid.element;
using Type = ExtendibleSokoban.model.levelsequence.level.grid.element.Type;

namespace ExtendibleSokoban.model.levelsequence.level
{
    /// <summary>
    /// An implementation of <see cref="ILevel"/>.
    /// </summary>
    class Level : ILevel, IEquatable<Level>
    {
        /// <summary>
        /// Creates an instance using the given name and grid.
        /// Note: doesn't validate the level.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="grid"></param>
        public Level(String name, IGrid grid)
        {
            this.Name = name;
            this.CurrentGrid = grid;
            this.InitialGrid = new Grid(grid);
        }

        /// <summary>
        /// The name of the level.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The grid of the level in its initial state.
        /// </summary>
        public IGrid InitialGrid { get; }

        /// <summary>
        /// The grid of the level in its current state.
        /// </summary>
        public IGrid CurrentGrid { get; }

        /// <summary>
        /// The user element.
        /// </summary>
        /// <returns>the user element</returns>
        public IElement User 
        {
            get
            {
                return this.CurrentGrid.Elements.Where(e => e.Type.Equals(Type.User)).First();
            } 
        }
        
        /// <summary>
        /// True if the level is finished, i.e. all the boxes are on a target.
        /// </summary>
        public bool IsFinished 
        {
            get
            {
                int coveredTargets = this.CurrentGrid.Elements
                    .Where(e => e.Type.Equals(Type.Box) || e.Type.Equals(Type.Target))
                    .Select(e => e.Position)
                    .Distinct()
                    .Count();
                int totalTargets = this.CurrentGrid.Elements.Where(e => e.Type.Equals(Type.Target)).Count();
                return totalTargets - coveredTargets == 0;
            }
        }

        /// <summary>
        /// Validates the level. It checks that the level has exactly one user, 
        /// at least one target and an equal number of targets and boxes.
        /// If not correct, it throws a LevelNotValidException. 
        /// </summary>
        public void Validate()
        {
            if (!ArePositionsValid())
            {
                throw new LevelNotValidException.UncorrectPositionException();
            }
            else
            {
                long nUsers = this.CurrentGrid.Elements.Where(e => e.Type.Equals(Type.User)).Count();
                long nBoxes = this.CurrentGrid.Elements.Where(e => e.Type.Equals(Type.Box)).Count();
                long nTargets = this.CurrentGrid.Elements.Where(e => e.Type.Equals(Type.Target)).Count();
                if (nUsers <= 0)
                {
                    throw new LevelNotValidException.NoInitialPointException();
                }
                else if (nUsers > 1)
                {
                    throw new LevelNotValidException.MultipleInitialPointException();
                }
                else if (nTargets <= 0)
                {
                    throw new LevelNotValidException.NoTargetException();
                }
                else if (nTargets != nBoxes)
                {
                    throw new LevelNotValidException.UnequalBoxAndTargetNumberException();
                }
            }
        }
        
        /// <summary>
        /// Two levels are equals if they have equal name and equal grid.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Level) obj);
        }

        /// <summary>
        /// Two levels are equals if they have equal name and equal grid.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Level other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Name == other.Name && InitialGrid.Equals(other.InitialGrid) && CurrentGrid.Equals(other.CurrentGrid);
        }

        /// <summary>
        /// The hash code of a level is computed on its name and grid.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Name.GetHashCode();
                hashCode = (hashCode * 397) ^ InitialGrid.GetHashCode();
                hashCode = (hashCode * 397) ^ CurrentGrid.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// Two levels are equals if they have equal name and equal grid.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(Level left, Level right)
        {
            return Equals(left, right);
        }

        /// <summary>
        /// Two levels are not equals if they don't have equal name and equal grid.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(Level left, Level right)
        {
            return !Equals(left, right);
        }

        /// <summary>
        /// Checks if all positions are in a valid range. It finds the maximum and
        /// minimum row and column position of all positions, and then checks they are in
        /// the correct range, e.g. minimum greater or equal to 0 and maximum less than number of rows
        /// </summary>
        /// <returns>true, if positions are valid</returns>
        private bool ArePositionsValid()
        {
            int rowMax = this.CurrentGrid.Elements.Select(e => e.Position.RowIndex).DefaultIfEmpty(0).Max();
            int rowMin = this.CurrentGrid.Elements.Select(e => e.Position.RowIndex).DefaultIfEmpty(0).Min();
            int colMax = this.CurrentGrid.Elements.Select(e => e.Position.ColumnIndex).DefaultIfEmpty(0).Max();
            int colMin = this.CurrentGrid.Elements.Select(e => e.Position.ColumnIndex).DefaultIfEmpty(0).Min();
            return Math.Max(rowMax, colMax) < this.CurrentGrid.Nrows && Math.Min(rowMin, colMin) >= 0;
        }
    }
}
