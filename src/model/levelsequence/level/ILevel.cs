using System;
using ExtendibleSokoban.model.levelsequence.level.grid;
using ExtendibleSokoban.model.levelsequence.level.grid.element;

namespace ExtendibleSokoban.model.levelsequence.level
{
    /// <summary>
    /// A Level of the game. Has a name and a grid, it can be validated and played.
    /// Note: Two levels are equals if they have equal name and equal grid.
    /// </summary>
    public interface ILevel
    {
        /// <summary>
        /// The name of the level.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// The grid of the level in its initial state.
        /// </summary>
        IGrid InitialGrid { get; }

        /// <summary>
        /// The grid of the level in its current state.
        /// </summary>
        IGrid CurrentGrid { get; }

        /// <summary>
        /// The user element.
        /// </summary>
        /// <returns></returns>
        IElement User { get; }

        /// <summary>
        /// True if the level is finished, i.e. all the boxes are on a target.
        /// </summary>
        bool IsFinished { get; }

        /// <summary>
        /// Validates the level. It checks that the level has exactly one user, 
        /// at least one target and an equal number of targets and boxes.
        /// If not correct, it throws a LevelNotValidException. 
        /// </summary>
        void Validate();
    }
}