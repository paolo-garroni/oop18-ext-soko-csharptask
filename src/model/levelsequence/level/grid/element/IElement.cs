namespace ExtendibleSokoban.model.levelsequence.level.grid.element
{
    /// <summary>
    /// An element of the game, which has a type and a position. Note: Two elements are equal if their position and type are equal. 
    /// </summary>
    public interface IElement
    {
        /// <summary>
        /// The type of the element.
        /// </summary>
        Type Type { get; }

        /// <summary>
        /// The position of the element.
        /// </summary>
        IPosition Position { get; set; }

        /// <summary>
        /// Checks if the element is movable (e.g. user or box).
        /// </summary>
        /// <returns>true, if type is movable</returns>
        bool IsTypeMovable { get; }

        /// <summary>
        /// Moves the element in a given direction if that movement is possible.
        /// </summary>
        /// <param name="direction">the direction of the movement</param>
        void Move(MovementDirection direction);
    }
}
