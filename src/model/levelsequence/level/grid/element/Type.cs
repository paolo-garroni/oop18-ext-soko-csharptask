namespace ExtendibleSokoban.model.levelsequence.level.grid.element
{
    /// <summary>
    /// The types of the elements.
    /// </summary>
    public enum Type
    {
        /// The user type.
        User,
        /// The box type.
        Box,
        /// The wall type.
        Wall,
        /// The target type.
        Target,
        /// The empty type.
        Empty,
    }
}