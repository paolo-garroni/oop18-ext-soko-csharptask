﻿namespace ExtendibleSokoban.model.levelsequence.level.grid.element
{
    /// <summary>
    /// An implementation of <see cref="IPosition"/>.
    /// </summary>
    public class Position : IPosition
    {
        /// <summary>
        /// Creates a new instance with the given row and column indexes.
        /// </summary>
        /// <param name="rowIndex">the row index</param>
        /// <param name="columnIndex">the column index</param>
        public Position(int rowIndex, int columnIndex)
        {
            RowIndex = rowIndex;
            ColumnIndex = columnIndex;
        }

        ///<summary>The row index</summary>
        public int RowIndex { get; }
        
        ///<summary>The column index</summary>
        public int ColumnIndex { get; }

        /// <summary>
        /// Two positions are equal if their row index and column index are equals.
        /// </summary>
        /// <param name="other">the object to compare</param>
        /// <returns></returns>
        protected bool Equals(Position other)
        {
            return RowIndex == other.RowIndex && ColumnIndex == other.ColumnIndex;
        }

        /// <summary>
        /// Two positions are equal if their row index and column index are equals.
        /// </summary>
        /// <param name="obj">the object to compare</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Position) obj);
        }

        /// <summary>
        /// The hash code of a position is computed on its row and column indexes.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return (RowIndex * 397) ^ ColumnIndex;
            }
        }

        /// <summary>
        /// Two positions are equal if their row index and column index are equals.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(Position left, Position right)
        {
            return Equals(left, right);
        }

        /// <summary>
        /// Two positions are not equal if their row index and column index are not equals.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(Position left, Position right)
        {
            return !Equals(left, right);
        }
    }
}
