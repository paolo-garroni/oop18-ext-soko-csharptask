namespace ExtendibleSokoban.model.levelsequence.level.grid.element
{
    /// <summary>
    /// The Position in the grid-like world, expressed as row index and column index.
    /// Note: Two positions are equal if their row index and column index are equals.
    /// </summary>
    public interface IPosition
    {
        ///<summary>The row index</summary>
        int RowIndex { get; }

        ///<summary>The column index</summary>
        int ColumnIndex { get; }
    }
}