﻿using System;

namespace ExtendibleSokoban.model.levelsequence.level.grid.element
{
    /// <summary>
    /// An implementation of <see cref="IElement"/>.
    /// </summary>
    class Element : IElement, IEquatable<Element>
    {
        private readonly IGrid _grid;

        /// <summary>
        /// Creates a new instance with the given type, position and grid
        /// </summary>
        /// <param name="type"></param>
        /// <param name="position"></param>
        /// <param name="grid"></param>
        public Element(Type type, IPosition position, IGrid grid)
        {
            this._grid = grid;
            Type = type;
            Position = position;
        }

        /// <summary>
        /// The type of the element.
        /// </summary>
        public Type Type { get; }

        /// <summary>
        /// The position of the element.
        /// </summary>
        public IPosition Position { get; set; }

        /// <summary>
        /// Checks if the element is movable (e.g. user or box).
        /// </summary>
        /// <returns>true, if type is movable</returns>
        public bool IsTypeMovable
        {
            get { return this.Type.Equals(Type.User) || this.Type.Equals(Type.Box); }
        }

        /// <summary>
        /// Moves the element in a given direction if that movement is possible.
        /// </summary>
        /// <param name="direction">the direction of the movement</param>
        public void Move(MovementDirection direction)
        {
            if (this.IsTypeMovable)
            {
                this._grid.MoveAttempt(this, direction);
            }
        }

        /// <summary>
        /// Two Elements are equal if they have equal position and type.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Element other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Type == other.Type && Equals(Position, other.Position);
        }

        /// <summary>
        /// Two Elements are equal if they have equal position and type.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Element) obj);
        }

        /// <summary>
        /// The element hash code is computed on its position and type.
        /// </summary>
        /// <returns>the hash code</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return ((int) Type * 397) ^ (Position != null ? Position.GetHashCode() : 0);
            }
        }

        /// <summary>
        /// Two Elements are equal if they have equal position and type.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(Element left, Element right)
        {
            return Equals(left, right);
        }

        /// <summary>
        /// Two Elements are not equal if they don't have equal position and type.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(Element left, Element right)
        {
            return !Equals(left, right);
        }
    }
}
