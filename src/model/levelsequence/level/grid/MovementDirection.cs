namespace ExtendibleSokoban.model.levelsequence.level.grid
{

    /// <summary>
    /// The directions of movement.
    /// </summary>
    public enum MovementDirection
    {
        Up,
        Down,
        Left,
        Right
    }
}