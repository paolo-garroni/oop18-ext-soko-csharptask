using System.Collections.Generic;
using ExtendibleSokoban.model.levelsequence.level.grid.element;

namespace ExtendibleSokoban.model.levelsequence.level.grid
{
    /// <summary>
    /// The squared grid in which the elements are.
    /// Note: Two grids are equal if their elements are the same.
    /// </summary>
    public interface IGrid
    {
        ///<summary>The number of rows, which is equal to the number of columns. </summary>
        int Nrows { get; }

        /// <summary>
        /// The elements of the grid.
        /// </summary>
        List<IElement> Elements { get; }

        /// <summary>
        /// The boxes that are on the same position of a target.
        /// </summary>
        List<IElement> BoxesOnTarget { get; }

        /// <summary>
        /// Adds the given element to the grid. The position information is held by the element itself.
        /// </summary>
        /// <param name="element">the element to be added to the grid</param>
        void Add(IElement element);

        /// <summary>
        /// Removes the given element from the grid.
        /// </summary>
        /// <param name="element">the element to be removed from the grid</param>
        void Remove(IElement element);

        /// <summary>
        /// Removes all the elements.
        /// </summary>
        void Clear();
        
        /// <summary>
        /// Gets all the elements that are currently at the given position. In a given position there can be at most
        /// two elements at a given time (e.g. box over target or user over target). So the returned list can contain
        /// 0 to 2 elements.
        /// </summary>
        /// <param name="position">the position to search into</param>
        /// <returns></returns>
        List<IElement> GetElementsAt(IPosition position);
        
        /// <summary>
        /// Moves, if possible, the given element to the adjacent position in the given
        /// direction. The only movable elements are users and boxes. Users can move if
        /// the target position is empty or if there is a box that can move in the same
        /// direction. A box can move only if the target position is empty or a target.
        /// </summary>
        /// <param name="element">the element to be moved</param>
        /// <param name="direction">the direction of movement</param>
        /// <returns>true, if the movement succeeded</returns>
        bool MoveAttempt(IElement element, MovementDirection direction);
    }
}