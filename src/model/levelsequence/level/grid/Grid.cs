﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtendibleSokoban.model.levelsequence.level.grid.element;
using Type = ExtendibleSokoban.model.levelsequence.level.grid.element.Type;

namespace ExtendibleSokoban.model.levelsequence.level.grid
{
    /// <summary>
    /// An implementation of <see cref="IGrid"/>.
    /// </summary>
    class Grid : IGrid, IEquatable<Grid>
    {
        /// <summary>
        /// Creates an empty instance.
        /// </summary>
        public Grid()
        {
            this.Elements = new List<IElement>();
        }
        
        /// <summary>
        /// Creates a new grid wich is a copy of the given grid.
        /// </summary>
        /// <param name="g"></param>
        public Grid(IGrid g)
        {
            this.Elements = new List<IElement>();
            g.Elements.ForEach(e => this.Elements.Add(new Element(
                e.Type,
                new Position(e.Position.RowIndex, e.Position.ColumnIndex), 
                this)));
        }

        ///<summary>The number of rows, which is equal to the number of columns. </summary>
        public int Nrows { get; } = 20;

        /// <summary>
        /// The elements of the grid.
        /// </summary>
        public List<IElement> Elements { get; }

        /// <summary>
        /// The boxes that are on the same position of a target.
        /// </summary>
        public List<IElement> BoxesOnTarget
        {
            get
            {
                List<IElement> boxes = this.Elements.Where(e => e.Type.Equals(Type.Box)).ToList();
                List<IElement> targets = this.Elements.Where(e => e.Type.Equals(Type.Target)).ToList();
                List<IElement> boxesOnTarget = new List<IElement>();
                boxes.ForEach(b => 
                {
                    targets.ForEach(t =>
                    {
                        if (b.Position.Equals(t.Position))
                        {
                            boxesOnTarget.Add(b);
                        }
                    });
                });
                return boxesOnTarget;
            }
        }

        /// <summary>
        /// Adds the given element to the grid. The position information is held by the element itself.
        /// </summary>
        /// <param name="element">the element to be added to the grid</param>
        public void Add(IElement element)
        {
            Elements.Add(element);
        }
        
        /// <summary>
        /// Removes the given element from the grid.
        /// </summary>
        /// <param name="element">the element to be removed from the grid</param>
        public void Remove(IElement element)
        {
            Elements.Remove(element);
        }

        /// <summary>
        /// Removes all the elements.
        /// </summary>
        public void Clear()
        {
            Elements.Clear();
        }

        /// <summary>
        /// Gets all the elements that are currently at the given position. In a given position there can be at most
        /// two elements at a given time (e.g. box over target or user over target). So the returned list can contain
        /// 0 to 2 elements.
        /// </summary>
        /// <param name="position">the position to search into</param>
        /// <returns></returns>
        public List<IElement> GetElementsAt(IPosition position)
        {
            return Elements.Where(e => e.Position.Equals(position)).ToList();
        }
        
        /// <summary>
        /// Moves, if possible, the given element to the adjacent position in the given
        /// direction. The only movable elements are users and boxes. Users can move if
        /// the target position is empty or if there is a box that can move in the same
        /// direction. A box can move only if the target position is empty or a target.
        /// </summary>
        /// <param name="element">the element to be moved</param>
        /// <param name="direction">the direction of movement</param>
        /// <returns>true, if the movement succeeded</returns>
        public bool MoveAttempt(IElement element, MovementDirection direction)
        {
            bool success = false;
            // only users and box can move
            if (element.IsTypeMovable)
            {
                var computeTargetPositionFunction = MovementDirectionExtensions.ComputeTargetPosition(direction);
                IPosition newPosition = computeTargetPositionFunction(element.Position);
                // continue only if target position is inside the grid
                if (Math.Max(newPosition.RowIndex, newPosition.ColumnIndex) < Nrows 
                    && Math.Min(newPosition.RowIndex, newPosition.ColumnIndex) >= 0)
                {
                    // find what obstacles are in the target position
                    List<IElement> obstacles = this.GetElementsAt(newPosition);
                    if (!obstacles.Any() || obstacles.All(o => o.Type.Equals(Type.Target)))
                    {
                        // no obstacles, move
                        element.Position = newPosition;
                        success = true;
                    }
                    else
                    {
                        obstacles.ForEach(o =>
                        {
                            if (element.Type.Equals(Type.User) && o.Type.Equals(Type.Box))
                            {
                                // this is the user and a box is the obstacle, try to move it
                                bool boxHasMoved = MoveAttempt(o, direction);
                                // the box will move only if its target position is empty
                                if (boxHasMoved)
                                {
                                    // if the box moved, move the user also
                                    element.Position = newPosition;
                                    success = true;
                                }
                            }
                        });
                    }
                }
            }
            return success;
        }

        /// <summary>
        /// Two grids are equal if their elements are the same.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Grid other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Elements.SequenceEqual(other.Elements);
        }
        
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Grid) obj);
        }

        /// <summary>
        /// The grid hash code is computed on grid elements.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (Elements != null ? Elements.GetHashCode() : 0);
        }

        public static bool operator ==(Grid left, Grid right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Grid left, Grid right)
        {
            return !Equals(left, right);
        }
    }
}
