﻿using System;
using ExtendibleSokoban.model.levelsequence.level.grid.element;

namespace ExtendibleSokoban.model.levelsequence.level.grid
{
    /// <summary>
    /// Extensions for the MovementDirection enum.
    /// </summary>
    public static class MovementDirectionExtensions
    {
        /// <summary>
        /// For each MovementDirection it returns a function to compute the target position of a movement
        /// (given an initial position).
        /// </summary>
        /// <param name="movementDirection">the MovementDirection</param>
        /// <returns></returns>
        public static Func<IPosition, IPosition> ComputeTargetPosition(this MovementDirection movementDirection)
        {
            Func<IPosition, IPosition> f = p => p;
            if (movementDirection.Equals(MovementDirection.Up))
            {
                f = p => new Position(p.RowIndex - 1, p.ColumnIndex);
            }
            else if (movementDirection.Equals(MovementDirection.Down))
            {
                f = p => new Position(p.RowIndex + 1, p.ColumnIndex);
            }
            else if (movementDirection.Equals(MovementDirection.Left))
            {
                f = p => new Position(p.RowIndex, p.ColumnIndex - 1);
            }
            else if (movementDirection.Equals(MovementDirection.Right))
            {
                f = p => new Position(p.RowIndex, p.ColumnIndex + 1);
            }
            return f;
        }
    }
}
