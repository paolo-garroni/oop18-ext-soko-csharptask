using System;

namespace ExtendibleSokoban.model.levelsequence.level
{
    public class LevelNotValidException : Exception
    {
        private const String BaseMessage = "Level not valid. ";
        private const String UncorrectPositionMessage = "Uncorrect position found.";
        private const String NoInitialPointMessage = "No initial point was found.";
        private const String MultipleInitialPointMessage = "Initial point is not singular.";
        private const String NoTargetMessage = "No target was found.";
        private const String UnequalBoxAndTargetMessage = "Boxes and targets quantity is not equal.";

        /// <summary>
        /// Returns the base error message. Sub-classes will include this message at the start of their own and
        /// more specific message.
        /// </summary>
        /// <returns>the base error message</returns>
        override public string ToString()
        {
            return BaseMessage;
        }

        /// <summary>
        /// The exception meaning that one or more positions are not in the grid.
        /// </summary>
        public class UncorrectPositionException : LevelNotValidException
        {
            override public string ToString()
            {
                return base.ToString() + UncorrectPositionMessage;
            }
        }

        /// <summary>
        /// The exception meaning no initial position was found.
        /// </summary>
        public class NoInitialPointException : LevelNotValidException
        {
            override public string ToString()
            {
                return base.ToString() + NoInitialPointMessage;
            }
        }

        /// <summary>
        /// The exception meaning that more than one initial position were found.
        /// </summary>
        public class MultipleInitialPointException : LevelNotValidException
        {
            override public string ToString()
            {
                return base.ToString() + MultipleInitialPointMessage;
            }
        }

        /// <summary>
        /// The Exception meaning no target was found.
        /// </summary>
        public class NoTargetException : LevelNotValidException
        {
            override public string ToString()
            {
                return base.ToString() + NoTargetMessage;
            }
        }

        /// <summary>
        /// The Exception meaning boxes and targets are not in equal number.
        /// </summary>
        public class UnequalBoxAndTargetNumberException : LevelNotValidException
        {
            override public string ToString()
            {
                return base.ToString() + UnequalBoxAndTargetMessage;
            }
        }
    }
}