using ExtendibleSokoban.model.levelsequence.level.grid.element;
using Xunit;
using Position = ExtendibleSokoban.model.levelsequence.level.grid.element.Position;

namespace ExtendibleSokoban.test.model
{
    /// <summary>
    /// Tests <see cref="IPosition"/>
    /// </summary>
    public class TestIPosition
    {
        /// <summary>
        /// Tests all IPosition methods.
        /// </summary>
        [Fact]
        public void TestAll()
        {
            IPosition p0 = new Position(0, 1);
            IPosition p1 = new Position(1, 2);
            IPosition p2 = new Position(2, 3);
            // get row index
            Assert.Equal(0, p0.RowIndex);
            Assert.Equal(1, p1.RowIndex);
            Assert.Equal(2, p2.RowIndex);
            // get column index
            Assert.Equal(1, p0.ColumnIndex);
            Assert.Equal(2, p1.ColumnIndex);
            Assert.Equal(3, p2.ColumnIndex);
            // equals
            Assert.Equal(p0, new Position(0, 1));
            Assert.Equal(p1, new Position(1, 2));
            Assert.Equal(p2, new Position(2, 3));
            Assert.NotEqual(p0, new Position(0, 0));
            Assert.NotEqual(p0, new Position(1, 1));
        }
    }
}