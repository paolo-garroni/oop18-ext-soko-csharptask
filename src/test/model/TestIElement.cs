﻿using ExtendibleSokoban.model.levelsequence.level.grid;
using ExtendibleSokoban.model.levelsequence.level.grid.element;
using Xunit;
using Type = ExtendibleSokoban.model.levelsequence.level.grid.element.Type;

namespace ExtendibleSokoban.test.model
{
    /// <summary>
    /// Tests <see cref="IElement"/>.
    /// </summary>
    public class TestIElement
    {
        private readonly IGrid _g;
        private readonly IElement _user;
        private readonly IElement _box;
        private readonly IElement _target;
        private readonly IElement _wall1;
        private readonly IElement _wall2;

        /// <summary>
        /// Creates a sample grid with sample elements.
        /// </summary>
        public TestIElement()
        {
            this._g = new Grid();
            this._user = new Element(Type.User, new Position(0, 0), _g);
            this._box = new Element(Type.Box, new Position(1, 1), _g);
            this._target = new Element(Type.Target, new Position(2, 2), _g);
            this._wall1 = new Element(Type.Wall, new Position(3, 3), _g);
            this._wall2 = new Element(Type.Wall, new Position(4, 4), _g);
            _g.Add(_user);
            _g.Add(_box);
            _g.Add(_target);
            _g.Add(_wall1);
            _g.Add(_wall2);
        }

        /// <summary>
        /// Tests Type property.
        /// </summary>
        [Fact]
        public void TestType()
        {
            Assert.Equal(Type.User, _user.Type);
            Assert.Equal(Type.Box, _box.Type);
            Assert.Equal(Type.Target, _target.Type);
            Assert.Equal(Type.Wall, _wall1.Type);
            Assert.Equal(Type.Wall, _wall2.Type);
        }

        /// <summary>
        /// Tests Position property.
        /// </summary>
        [Fact]
        public void TestPosition()
        {
            Assert.Equal(new Position(0, 0), _user.Position);
            Assert.Equal(new Position(1, 1), _box.Position);
            Assert.Equal(new Position(2, 2), _target.Position);
            Assert.Equal(new Position(3, 3), _wall1.Position);
            Assert.Equal(new Position(4, 4), _wall2.Position);
            _user.Position = new Position(0, 1);
            Assert.Equal(new Position(0, 1), _user.Position);
            _user.Position = new Position(0, 0);
            Assert.Equal(new Position(0, 0), _user.Position);
        }

        /// <summary>
        /// Tests IsTypeMovable method.
        /// </summary>
        [Fact]
        public void TestIsTypeMovable()
        {
            Assert.True(_user.IsTypeMovable);
            Assert.True(_box.IsTypeMovable);
            Assert.False(_target.IsTypeMovable);
            Assert.False(_wall1.IsTypeMovable);
            Assert.False(_wall2.IsTypeMovable);
        }

        /// <summary>
        /// Tests equal method.
        /// </summary>
        [Fact]
        public void TestEquals()
        {
            Element user2 = new Element(Type.User, new Position(0, 0), _g);
            Assert.Equal(_user, user2);
            user2.Position = new Position(0, 1);
            Assert.NotEqual(_user, user2);
            Element user3 = new Element(Type.Wall, new Position(0, 0), _g);
            Assert.NotEqual(_user, user3);
            Grid g2 = new Grid();
            g2.Add(user3);
            Element user4 = new Element(Type.User, new Position(0, 1), g2);
            Assert.NotEqual(_user, user4);
        }

        /// <summary>
        /// Tests move method.
        /// </summary>
        [Fact]
        public void TestMove()
        {
            // test free movement in all directions using user
            Assert.Equal(new Position(0, 0), _user.Position);
            _user.Move(MovementDirection.Right);
            Assert.Equal(new Position(0, 1), _user.Position);
            _user.Move(MovementDirection.Down); // user should have pushed box down at 2,1
            Assert.Equal(new Position(1, 1), _user.Position);
            Assert.Equal(new Position(2, 1), _box.Position);
            _user.Move(MovementDirection.Left);
            Assert.Equal(new Position(1, 0), _user.Position);
            _user.Move(MovementDirection.Up);
            Assert.Equal(new Position(0, 0), _user.Position);
            // test box movement
            _box.Position = new Position(1, 1);
            Assert.Equal(new Position(1, 1), _box.Position);
            _box.Move(MovementDirection.Down);
            Assert.Equal(new Position(2, 1), _box.Position);
            // test box over target movement
            _box.Move(MovementDirection.Right);
            Assert.Equal(new Position(2, 2), _box.Position);
            Assert.Equal(_target.Position, _box.Position);
            // test box over wall non-movement
            _box.Move(MovementDirection.Right);
            Assert.Equal(new Position(2, 3), _box.Position);
            _box.Move(MovementDirection.Down);
            Assert.NotEqual(new Position(3, 3), _box.Position);
            Assert.Equal(new Position(2, 3), _box.Position);
            // test user over wall non-movement
            _user.Move(MovementDirection.Down);
            _user.Move(MovementDirection.Down);
            _user.Move(MovementDirection.Down);
            _user.Move(MovementDirection.Right);
            _user.Move(MovementDirection.Right);
            _user.Move(MovementDirection.Right);
            Assert.NotEqual(new Position(3, 3), _user.Position);
            Assert.Equal(new Position(3, 2), _user.Position);
        }
    }
}
