﻿using System.Collections.Generic;
using System.Linq;
using ExtendibleSokoban.model;
using ExtendibleSokoban.model.levelsequence;
using ExtendibleSokoban.model.levelsequence.level;
using ExtendibleSokoban.model.levelsequence.level.grid;
using ExtendibleSokoban.model.levelsequence.level.grid.element;
using Xunit;

namespace ExtendibleSokoban.test.model
{
    /// <summary>
    /// Tests <see cref="IModel"/>.
    /// </summary>
    public class TestIModel
    {
        private readonly IModel _model;
        private readonly List<ILevel> _levelList;
        private readonly ILevel _l1;
        private readonly ILevel _l2;
        private readonly ILevel _l3;

        /// <summary>
        /// Creates a level list with three sample levels.
        /// </summary>
        public TestIModel()
        {
            this._model = new Model();
            this._l1 = new Level("Level 1", new Grid());
            this._l2 = new Level("Level 2", new Grid());
            this._l3 = new Level("Level 3", new Grid());
            this._levelList = new List<ILevel>();
            this._levelList.Add(_l1);
            this._levelList.Add(_l2);
            this._levelList.Add(_l3);
        }

        /// <summary>
        /// Tests Add method.
        /// </summary>
        [Fact]
        public void TestAdd()
        {
            // At creation, the level sequence is empty
            Assert.True(!_model.LevelSequence.Levels.Any());
            Assert.True(!_model.LevelSequenceInitialState.Levels.Any());
            // but we can set one
            LevelSequence ls = new LevelSequence("My level sequence", _levelList);
            _model.LevelSequence = ls;
            Assert.False(!_model.LevelSequence.Levels.Any());
            Assert.False(!_model.LevelSequenceInitialState.Levels.Any());
            Assert.Equal(_l1.Name, _model.LevelNames.ElementAt(0));
            Assert.Equal(_l2.Name, _model.LevelNames.ElementAt(1));
            Assert.Equal(_l3.Name, _model.LevelNames.ElementAt(2));
        }

        /// <summary>
        /// Tests all IModel methods.
        /// </summary>
        [Fact]
        public void TestModel()
        {
            ILevelSequence ls = new LevelSequence("My level sequence", _levelList);
            _model.LevelSequence = ls;
            ILevelSequence lsInitialState = _model.LevelSequence.CreateCopy();
            Assert.Equal(lsInitialState, _model.LevelSequenceInitialState);
            ls.SetNextLevelAsCurrent();
            Assert.NotEqual(lsInitialState, _model.LevelSequence);
            ls.SetNextLevelAsCurrent();
            IGrid currentGrid = _model.LevelSequence.CurrentLevel.CurrentGrid;
            currentGrid.Add(new Element(Type.User, new Position(0, 0), currentGrid));
            Assert.NotEqual(lsInitialState, _model.LevelSequence);
            Assert.Equal(lsInitialState, _model.LevelSequenceInitialState);
        }
    }
}
