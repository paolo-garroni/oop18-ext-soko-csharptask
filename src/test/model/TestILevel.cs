﻿using System.Collections.Generic;
using ExtendibleSokoban.model.levelsequence.level;
using ExtendibleSokoban.model.levelsequence.level.grid;
using ExtendibleSokoban.model.levelsequence.level.grid.element;
using Xunit;
using Type = ExtendibleSokoban.model.levelsequence.level.grid.element.Type;

namespace ExtendibleSokoban.test.model
{
    /// <summary>
    /// Tests <see cref="ILevel"/>.
    /// </summary>
    public class TestILevel
    {
        private readonly IGrid _g;
        private readonly IElement _box;
        private readonly IElement _wall;
        private readonly List<IElement> _elementList;
        
        /// <summary>
        /// Creates a level list with three sample levels.
        /// </summary>
        public TestILevel()
        {
            IElement target = new Element(Type.Target, new Position(2, 2), _g);
            IElement user = new Element(Type.User, new Position(0, 0), _g);
            this._g = new Grid();
            this._box = new Element(Type.Box, new Position(1, 1), _g);
            this._wall = new Element(Type.Wall, new Position(3, 3), _g);
            this._elementList = new List<IElement>();
            this._elementList.Add(user);
            this._elementList.Add(_box);
            this._elementList.Add(_wall);
            this._elementList.Add(target);
            this._g.Add(user);
            this._g.Add(_box);
            this._g.Add(_wall);
            this._g.Add(target);
        }

        [Fact]
        public void TestName()
        {
            Assert.Equal("My name", new Level("My name", new Grid()).Name);
        }

        [Fact]
        public void TestInitialVsCurrent()
        {
            Level l = new Level("My level", new Grid(_g));
            Assert.Equal(l.CurrentGrid, l.InitialGrid);
            l.User.Move(MovementDirection.Right);
            Assert.NotEqual(l.CurrentGrid, l.InitialGrid);
            l.User.Move(MovementDirection.Left);
            Assert.Equal(l.CurrentGrid, l.InitialGrid);
        }

        [Fact]
        public void TestUser()
        {
            Grid g2 = new Grid();
            Element user = new Element(Type.User, new Position(0, 0), g2);
            g2.Add(user);
            Level l = new Level("My level", g2);
            l.User.Move(MovementDirection.Right);
            Assert.True(l.User.Position.Equals(new Position(0, 1)));
            Assert.Equal(l.User, user);
            Assert.True(l.User == user);
            l.User.Move(MovementDirection.Right);
            Assert.Equal(l.User, user);
            Assert.True(l.User == user);
        }

        [Fact]
        public void TestIsFinished()
        {
            Grid g2 = new Grid(_g);
            g2.Add(new Element(Type.Target, new Position(1, 2), g2));
            g2.Add(new Element(Type.Box, new Position(2, 1), g2));
            Level l = new Level("My level", g2);
            Assert.False(l.IsFinished);
            l.User.Move(MovementDirection.Down);
            l.User.Move(MovementDirection.Right);
            Assert.False(l.IsFinished);
            l.User.Move(MovementDirection.Left);
            l.User.Move(MovementDirection.Down);
            l.User.Move(MovementDirection.Right);
            Assert.True(l.IsFinished);
        }

        [Fact]
        public void TestValidate()
        {
            // Grid g is an example of a correct grid: must succeed
            Grid g2 = new Grid(_g);
            Level l2 = new Level("My level", g2);
            try
            {
                l2.Validate();
            }
            catch (LevelNotValidException)
            {
                Assert.True(false);
            }
            // empty grid must Assert.Fail
            Grid g3 = new Grid();
            Level l3 = new Level("My level", g3);
            try
            {
                l3.Validate();
                Assert.True(false);
            }
            catch (LevelNotValidException) {}
            // grid with no user must Assert.Fail
            Grid g4 = new Grid();
            g4.Add(new Element(Type.Target, new Position(0, 0), g4));
            Level l4 = new Level("My level", g4);
            try
            {
                l4.Validate();
                Assert.True(false);
            }
            catch (LevelNotValidException){}
            // grid with no target must Assert.Fail
            Grid g5 = new Grid();
            g5.Add(new Element(Type.User, new Position(0, 0), g5));
            Level l5 = new Level("My level", g4);
            try
            {
                l5.Validate();
                Assert.True(false);
            }
            catch (LevelNotValidException){}
            // grid with more than one user must Assert.Fail
            Grid g6 = new Grid();
            g6.Add(new Element(Type.User, new Position(0, 0), g6));
            g6.Add(new Element(Type.User, new Position(1, 1), g6));
            Level l6 = new Level("My level", g6);
            try
            {
                l6.Validate();
                Assert.True(false);
            }
            catch (LevelNotValidException){}
            // grid with an unequal number of targets and boxes must Assert.Fail
            Grid g7 = new Grid();
            g7.Add(new Element(Type.Target, new Position(0, 1), g7));
            g7.Add(new Element(Type.Target, new Position(1, 0), g7));
            g7.Add(new Element(Type.Box, new Position(1, 1), g7));
            g7.Add(new Element(Type.Box, new Position(0, 2), g7));
            g7.Add(new Element(Type.Box, new Position(2, 0), g7));
            g7.Add(new Element(Type.User, new Position(2, 2), g7));
            Level l7 = new Level("My level", g7);
            try
            {
                l7.Validate();
                Assert.True(false);
            }
            catch (LevelNotValidException){}
        }

        [Fact]
        public void TestEquals()
        {
            Assert.NotEqual(new Level("My name", _g), new Level("My different name", _g));
            Assert.NotEqual(new Level("My name", _g), new Level("My name", new Grid()));
            Assert.Equal(new Level("My name", _g), new Level("My name", _g));
        }
    }
}
