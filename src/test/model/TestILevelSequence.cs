﻿using System.Collections.Generic;
using System.Linq;
using ExtendibleSokoban.model.levelsequence;
using ExtendibleSokoban.model.levelsequence.level;
using ExtendibleSokoban.model.levelsequence.level.grid;
using ExtendibleSokoban.model.levelsequence.level.grid.element;
using Xunit;

namespace ExtendibleSokoban.test.model
{
    public class TestILevelSequence
    {
        private readonly List<ILevel> _levelList;
        private readonly ILevel _l1;
        private readonly ILevel _l2;
        private readonly ILevel _l3;

        /// <summary>
        /// Creates a level list with three sample levels.
        /// </summary>
        public TestILevelSequence()
        {
            this._levelList = new List<ILevel>();
            this._l1 = new Level("Level 1", new Grid());
            this._l2 = new Level("Level 2", new Grid());
            this._l3 = new Level("Level 3", new Grid());
            this._levelList.Add(_l1);
            this._levelList.Add(_l2);
            this._levelList.Add(_l3);
        }

        /// <summary>
        /// Tests creation.
        /// </summary>
        [Fact]
        public void TestCreation()
        {
            ILevelSequence ls = new LevelSequence("My level-Sequence Name!");
            ILevelSequence ls2 = new LevelSequence("My level-Sequence Name!");
            Assert.Equal(ls, ls2);
            List<ILevel> levelList2 = new List<ILevel>();
            _levelList.ForEach(levelList2.Add);
            ls = new LevelSequence("", _levelList);
            ls2 = new LevelSequence("", _levelList);
            Assert.Equal(ls, ls2);
        }

        /// <summary>
        /// Tests Swap method.
        /// </summary>
        [Fact]
        public void TestSwap()
        {
            ILevelSequence ls = new LevelSequence("", _levelList);
            List<ILevel> levelList2 = new List<ILevel>();
            _levelList.ForEach(levelList2.Add);
            ILevelSequence ls2 = new LevelSequence("", levelList2);
            ls.Swap(0, 2);
            Assert.NotEqual(ls2, ls);
            ILevel tmp = levelList2[0];
            levelList2[0] = levelList2[2];
            levelList2[2] = tmp;
            Assert.Equal(new LevelSequence("", levelList2), ls);
        }

        /// <summary>
        /// Tests Remove method.
        /// </summary>
        [Fact]
        public void TestRemove()
        {
            ILevelSequence ls = new LevelSequence("", _levelList);
            List<ILevel> levelList2 = new List<ILevel>();
            _levelList.ForEach(levelList2.Add);
            ILevelSequence ls2 = new LevelSequence("", levelList2);
            // Remove
            ls.Remove(1);
            Assert.NotEqual(ls, ls2);
            ls2.Remove(1);
            Assert.Equal(ls, ls2);
            // Clear
            ls.Clear();
            Assert.Equal(new LevelSequence("", new List<ILevel>()), ls);
            Assert.NotEqual(ls2, ls);
            levelList2.Clear();
            Assert.Equal(ls2, ls);
            // Add
            ls2.Add(_l1);
            ls.Add(_l1);
            Assert.Equal(ls2, ls);
        }

        /// <summary>
        /// Tests CreateCopy method.
        /// </summary>
        [Fact]
        public void TestCreateCopy()
        {
            ILevelSequence ls = new LevelSequence("My level sequence", _levelList);
            ILevelSequence lsCopy = ls.CreateCopy();
            Enumerable.Range(0, 2).ToList().ForEach(i => {
                Assert.True(ls.Levels.ElementAt(i) != lsCopy.Levels.ElementAt(i));
            });
        }

        /// <summary>
        /// Tests level iteration methods.
        /// </summary>
        [Fact]
        public void TestLevelIteration()
        {
            ILevelSequence ls = new LevelSequence("My level sequence", _levelList).CreateCopy();
            Assert.True(ls.SetNextLevelAsCurrent());
            Assert.Equal(ls.CurrentLevel, _l1);
            Assert.True(ls.SetNextLevelAsCurrent());
            Assert.Equal(ls.CurrentLevel, _l2);
            Assert.True(ls.SetNextLevelAsCurrent());
            Assert.Equal(ls.CurrentLevel, _l3);
            Assert.False(ls.SetNextLevelAsCurrent());
        }

        /// <summary>
        /// Tests RestartCurrentLevel method.
        /// </summary>
        [Fact]
        public void TestLevelRestart()
        {
            List<ILevel> levelList = new List<ILevel>();
            ILevel l1 = new Level("Level 1", new Grid());
            levelList.Add(l1);
            ILevelSequence ls = new LevelSequence("My level sequence", levelList);
            ls.SetNextLevelAsCurrent();
            ls.CurrentLevel.CurrentGrid.GetElementsAt(new Position(0, 0)).ForEach(e => {
                e.Position = new Position(1, 1);
            });
            ls.CurrentLevel.CurrentGrid.Elements.ForEach(e => {
                Assert.False(e.Position.Equals(new Position(0, 0)));
            });
            ls.RestartCurrentLevel();
            ls.CurrentLevel.CurrentGrid.Elements.ForEach(e => {
                Assert.True(e.Position.Equals(new Position(0, 0)));
            });
        }
    }
}
