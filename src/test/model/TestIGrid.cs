﻿using System.Collections.Generic;
using System.Linq;
using ExtendibleSokoban.model.levelsequence.level.grid;
using ExtendibleSokoban.model.levelsequence.level.grid.element;
using Xunit;
using Type = ExtendibleSokoban.model.levelsequence.level.grid.element.Type;

namespace ExtendibleSokoban.test.model
{
    /// <summary>
    /// Tests <see cref="IGrid"/>.
    /// </summary>
    public class TestIGrid
    {
        private readonly IGrid _g;
        private readonly IElement _user;
        private readonly IElement _box;
        private readonly IElement _target;
        private readonly IElement _wall;
        private readonly List<IElement> _elementList;

        /// <summary>
        /// Creates a three sample elements, inserts them in a list and in a grid.
        /// </summary>
        public TestIGrid()
        {
            this._g = new Grid();
            this._user = new Element(Type.User, new Position(0, 0), _g);
            this._box = new Element(Type.Box, new Position(1, 1), _g);
            this._wall = new Element(Type.Wall, new Position(2, 2), _g);
            this._target = new Element(Type.Target, new Position(3, 3), _g);
            this._elementList = new List<IElement>();
            this._elementList.Add(_user);
            this._elementList.Add(_box);
            this._elementList.Add(_wall);
            this._elementList.Add(_target);
            _g.Add(_user);
            _g.Add(_box);
            _g.Add(_wall);
            _g.Add(_target);
        }

        /// <summary>
        /// Tests creation.
        /// </summary>
        [Fact]
        public void TestCreation()
        {
            // empty grid
            Assert.True(!new Grid().Elements.Any());
            // grid copy
            Grid g2 = new Grid(this._g);
            Assert.False(!g2.Elements.Any());
            Assert.Equal(4, g2.Elements.Count());
            Assert.True(_g != g2);
            Assert.True(_g.Elements != g2.Elements);
            _g.Elements.ForEach(e1 => {
                g2.Elements.ForEach(e2 => {
                    Assert.True(e1 != e2);
                });
            });
            Enumerable.Range(0, 3).ToList().ForEach(i=> {
                Assert.True(_g.Elements.ElementAt(i).Equals(g2.Elements.ElementAt(i)));
            });
        }

        /// <summary>
        /// Tests RemoveElement method.
        /// </summary>
        [Fact]
        public void TestRemoveElement()
        {
            Grid g2 = new Grid(this._g);
            int i = 4;
            Assert.Equal(i, g2.Elements.Count());
            g2.Remove(_user);
            i -= 1;
            Assert.Equal(i, g2.Elements.Count());
            g2.Remove(_box);
            i -= 1;
            Assert.Equal(i, g2.Elements.Count());
            g2.Remove(_wall);
            i -= 1;
            Assert.Equal(i, g2.Elements.Count());
            g2.Remove(_target);
            i -= 1;
            Assert.Equal(i, g2.Elements.Count());
            g2.Remove(new Element(Type.Empty, new Position(0, 0), g2));
            Assert.Equal(i, g2.Elements.Count());
        }

        /// <summary>
        /// Tests AddElement method.
        /// </summary>
        [Fact]
        public void TestAddElement()
        {
            Grid g2 = new Grid();
            int i = 0;
            Assert.Equal(i, g2.Elements.Count());
            g2.Add(_user);
            i += 1;
            Assert.Equal(i, g2.Elements.Count());
            g2.Add(_box);
            i += 1;
            Assert.Equal(i, g2.Elements.Count());
            g2.Add(_wall);
            i += 1;
            Assert.Equal(i, g2.Elements.Count());
            g2.Add(_target);
            i += 1;
            Assert.Equal(i, g2.Elements.Count());
            g2.Add(new Element(Type.Empty, new Position(0, 0), g2));
            i += 1;
            Assert.Equal(i, g2.Elements.Count());
        }

        /// <summary>
        /// Tests Clear method.
        /// </summary>
        [Fact]
        public void TestClear()
        {
            Grid g2 = new Grid(_g);
            Assert.Equal(4, g2.Elements.Count());
            g2.Clear();
            Assert.Equal(0, g2.Elements.Count());
        }

        /// <summary>
        /// Tests Element property.
        /// </summary>
        [Fact]
        public void TestElements()
        {
            IGrid g2 = new Grid(_g);
            Assert.Equal(_g, g2);
        }

        /// <summary>
        /// Test BoxesOnTarget property.
        /// </summary>
        [Fact]
        public void TestBoxesOnTarget()
        {
            Grid g2 = new Grid(_g);
            Assert.Equal(0, g2.BoxesOnTarget.Count());
            g2.Add(new Element(Type.Box, new Position(3, 3), g2));
            Assert.Equal(1, g2.BoxesOnTarget.Count());
            g2.Add(new Element(Type.Target, new Position(3, 4), g2));
            Assert.Equal(1, g2.BoxesOnTarget.Count());
            IElement b = new Element(Type.Box, new Position(3, 4), g2);
            g2.Add(b);
            Assert.Equal(2, g2.BoxesOnTarget.Count());
            g2.Remove(b);
            Assert.Equal(1, g2.BoxesOnTarget.Count());
        }

        /// <summary>
        /// Tests GetElementAt method.
        /// </summary>
        [Fact]
        public void TestGetElementsAt()
        {
            Grid g2 = new Grid(_g);
            Assert.Equal(this._user, g2.GetElementsAt(new Position(0, 0)).ElementAt(0));
            Assert.Equal(this._box, g2.GetElementsAt(new Position(1, 1)).ElementAt(0));
            Assert.Equal(this._wall, g2.GetElementsAt(new Position(2, 2)).ElementAt(0));
            Assert.Equal(this._target, g2.GetElementsAt(new Position(3, 3)).ElementAt(0));
            g2.Add(new Element(Type.Target, new Position(3, 3), g2));
            Assert.Equal(2, g2.GetElementsAt(new Position(3, 3)).Count());
        }

        /// <summary>
        /// Tests MoveAttempt method.
        /// </summary>
        [Fact]
        public void TestMoveAttempt()
        {
            Grid g2 = new Grid(_g);
            IElement u = g2.GetElementsAt(new Position(0, 0)).ElementAt(0);
            g2.MoveAttempt(u, MovementDirection.Right);
            g2.MoveAttempt(u, MovementDirection.Down);
            g2.MoveAttempt(u, MovementDirection.Right);
            g2.MoveAttempt(u, MovementDirection.Right);
            g2.MoveAttempt(u, MovementDirection.Down);
            g2.MoveAttempt(u, MovementDirection.Down);
            g2.MoveAttempt(u, MovementDirection.Left);
            g2.MoveAttempt(u, MovementDirection.Up);
            Assert.Equal(new Position(3, 2), u.Position);
            Enumerable.Range(0, 10).ToList().ForEach(i=> {
                g2.MoveAttempt(u, MovementDirection.Left);
            });
            Assert.Equal(new Position(3, 0), u.Position);
            Enumerable.Range(0, 10).ToList().ForEach(i=> {
                g2.MoveAttempt(u, MovementDirection.Up);
            });
            Assert.Equal(new Position(0, 0), u.Position);

        }

        /// <summary>
        /// Tests equals.
        /// </summary>
        [Fact]
        public void TestEquals()
        {
            IGrid g1 = new Grid(_g);
            IGrid g2 = new Grid(_g);
            Assert.True(g2 != g1);
            Assert.Equal(g2, g1);
            g1.Elements.ForEach(e1=> {
                g2.Elements.ForEach(e2=> {
                    Assert.True(e1 != e2);
                });
            });
            Enumerable.Range(0, 3).ToList().ForEach(i =>
            {
                Assert.True(g1.Elements.ElementAt(i).Equals(g2.Elements.ElementAt(i)));
            });
            g1.Add(new Element(Type.Box, new Position(10, 10), g1));
            Assert.NotEqual(g1, g2);
        }
    }
}
